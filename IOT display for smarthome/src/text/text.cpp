#include "./text.h"
#include <ArduinoJson.h>

#include <string>
void loopingText(int r, int g, int b, int doubled, std::string text, int loops, RGBmatrixPanel4 *matrix,int dly){
      int index = 1;
      int loop=1;
      int textLen = text.length() + 2;
      std::string substring = text;
      while(true){
        if(index == 1 && loop == 1){
          substring = substring + "  ";
        }
        if(index == 2 && loop == 1){
          delay(1500);
        }
        matrix->setTextColor(matrix->Color888(r * (doubled+1),g * (doubled+1),b * (doubled+1)));
        matrix->setCursor(0,0);
        matrix->print(substring.c_str());
       
        delay(dly);
        matrix->swapBuffers(true);
        matrix->fillScreen(0);
        if(loop != loops){
        substring = substring + substring[0];
        }
        substring.erase(0,1);
        if(loop == loops  && index == textLen ){
          break;
        }
        if(index < textLen){
          index ++;
        }else{
          index = 1;
          loop ++;
        }
        if(loop == loops &&  index == 1){
          substring = text;
        }
  }
}
void scrollingText(int r, int g, int b, int doubled, std::string text, RGBmatrixPanel4 *matrix,int dly){
 int index = 1;
    int textLen = text.length();
    std::string substring = text;
    while(true){
      if(index != 1){
        substring.erase(0,1);
      }
      if(index == 2){
              delay(1500);
      }
      matrix->setTextColor(matrix->Color888(r * (doubled+1),g * (doubled+1),b * (doubled+1)));
      matrix->setCursor(0,0);
      matrix->print(substring.c_str());
    delay(dly);

      matrix->swapBuffers(true);
      matrix->fillScreen(0);

      if(index <= textLen){
          index ++;
        }else{
          delay(1500);
          break;
        }
        
    }
}
void scrollingTextChangeColor(std::string text, RGBmatrixPanel4 *matrix,int dly, int r, int g, int b, int doubled, int rmode, int gmode, int bmode){
 int index = 1;
    int textLen = text.length();
    std::string substring = text;
    while(true){
      if(index != 1){
        substring.erase(0,1);
      }
      if(index == 2){
              delay(1500);

      }
      matrix->setTextColor(matrix->Color888(r * (doubled+1),g * (doubled+1),b * (doubled+1)));
      matrix->setCursor(0,0);
      matrix->print(substring.c_str());
      matrix->setCursor(0,8);
      std::string test = "r" + std::to_string(rmode) + " g" + std::to_string(gmode) +" b" + std::to_string(bmode);
      matrix->print(test.c_str());      
            delay(dly);
      matrix->swapBuffers(true);
      matrix->fillScreen(0);

      if(index <= textLen){
          index ++;
        }else{
          delay(1500);

          break;
        }
        
    }
}

void loopedScrollingText(int r, int g, int b, int doubled, std::string text, int loops, RGBmatrixPanel4 *matrix,int dly){
    int index = 1;
    int loop = 1;
    int textLen = text.length();
    std::string substring = text;
    while(true){
      if(index != 1){
        substring.erase(0,1);
      }

      if(index == 2){
              delay(1500);

      }
      matrix->setTextColor(matrix->Color888(r * (doubled+1),g * (doubled+1),b * (doubled+1)));
      matrix->setCursor(0,0);
      matrix->print(substring.c_str());

      matrix->setTextColor(matrix->Color888(50,0,0));
      delay(dly);
      matrix->swapBuffers(true);
      matrix->fillScreen(0);
      if(loop == loops  && index > textLen ){
              delay(1500);
          break;
        }
      if(index <= textLen){
          index ++;
        }else{
          index = 1;
          loop ++;
          substring = text;
          delay(1500);


        }
        
    }
}
void displayUntilCancelled(int r, int g, int b, std::string text, RGBmatrixPanel4 *matrix,int dly, std::string *activityFlag, std::string activity){
   int index = 1;
    int textLen = text.length();
    std::string substring = text;
    while(*activityFlag == activity){
      if(index != 1){
        substring.erase(0,1);
      }
      if(index == 2){
              delay(1500);

      }
      matrix->setTextColor(matrix->Color888(r,g,b));
      matrix->setCursor(0,0);
      matrix->print(substring.c_str());
      matrix->setTextColor(matrix->Color888(50,0,0));
      delay(dly);
      matrix->swapBuffers(true);
      matrix->fillScreen(0);
      if(index <= textLen){
          index ++;
        }else{
          index = 1;
          substring = text;
          delay(1500);
        }
    }
      matrix->fillScreen(0);
            matrix->swapBuffers(true);
}
void displayClockUntilCancelled(int r, int g, int b, NTPClient timeClient, RGBmatrixPanel4 *matrix,int dly, std::string *activityFlag, std::string activity){
    while(*activityFlag == activity){
      matrix->setTextColor(matrix->Color888(r,g,b));
      matrix->setCursor(0,0);
      matrix->print(timeClient.getFormattedTime());
      matrix->setTextColor(matrix->Color888(50,0,0));
      delay(10);
      matrix->swapBuffers(true);
      matrix->fillScreen(0);
     
    }
      matrix->fillScreen(0);
      matrix->swapBuffers(true);
}
void displayOnce(int r, int g, int b, int *value, RGBmatrixPanel4 *matrix){
        matrix->fillScreen(0);

      matrix->setTextColor(matrix->Color888(r,g,b));
      matrix->setCursor(0,0);
      matrix->print(*value);
      matrix->setTextColor(matrix->Color888(50,0,0));
            matrix->swapBuffers(true);

}
void displayUntilCancelledEmphasised(int r, int g, int b, std::string text, RGBmatrixPanel4 *matrix,int dly, std::string *activityFlag, std::string activity){
   int index = 1;
    int textLen = text.length();
    std::string substring = text;
    while(*activityFlag == activity){
        matrix->drawRect(0,0,64,16, matrix->Color888(r,g,b));

      if(index != 1){
        substring.erase(0,1);
      }
      if(index == 2){
              delay(1500);

      }
      matrix->setTextColor(matrix->Color888(r,g,b));
      matrix->setCursor(1,1);
      matrix->print(substring.c_str());
      matrix->setTextColor(matrix->Color888(50,0,0));
      delay(dly);
      matrix->swapBuffers(true);
      matrix->fillScreen(0);
      if(index <= textLen){
          index ++;
        }else{
          index = 1;
          substring = text;
          delay(1500);
        }
    }
      matrix->fillScreen(0);
}

void displayOnceUntilCancelled(int r, int g, int b, std::string text, RGBmatrixPanel4 *matrix,int dly, std::string *activityFlag, std::string activity){
   int index = 1;
    int textLen = text.length();
    std::string substring = text;
    while((*activityFlag == activity) ){
      if(index != 1){
        substring.erase(0,1);
      }
      if(index == 2){
              delay(1500);

      }
      matrix->setTextColor(matrix->Color888(r,g,b));
      matrix->setCursor(0,0);
      matrix->print(substring.c_str());
      matrix->setTextColor(matrix->Color888(50,0,0));
      delay(dly);
      matrix->swapBuffers(true);
      matrix->fillScreen(0);
      if(index <= textLen){
          index ++;
        }else{
          delay(500);
                    break;

        }
    }
      matrix->fillScreen(0);
            matrix->swapBuffers(true);
}

void displayWeather(int r, int g, int b, RGBmatrixPanel4 *matrix,int dly, std::string *activityFlag, std::string activity, weatherStruct *weather){
  //descriptions
   int index = 1;
    int textLen = weather->descriptions.length();
    std::string substring = weather->descriptions;
    //end descriptions
    //details
    int temp = std::stoi(weather->temp) - 273.15;
      std::string detString = std::to_string(temp) + "'C | Wind Speed " + weather->windspeed+"M/S Gusts " + weather->windgust+"M/S | Clouds " + weather->clouds+"% |";
      std::string substring2 = detString;
    int index2 = 1;
    int textLen2 = detString.length();
    //end details
    while(*activityFlag == activity){
        matrix->fillScreen(0);
      //details
    if(index2 != 1){
        substring2.erase(0,1);
      }

      matrix->setTextColor(matrix->Color888(r,g,b));
      matrix->setCursor(0,0);
      matrix->print(substring2.c_str());
      matrix->setTextColor(matrix->Color888(50,0,0));
      matrix->swapBuffers(true);
      if(index2 <= textLen2){
          index2 ++;
        }else{
          index2 = 1;
          substring2 = std::to_string(temp) + "'C | Wind Speed " + weather->windspeed+"M/S Gusts " + weather->windgust+"M/S | Clouds " + weather->clouds+"% |";;
        }
      //weather details
      //sunrise
      //sunset
      //rain
      //snow
      //end details
      //descriptions
      if(index != 1){
        substring.erase(0,1);
      }

      matrix->setTextColor(matrix->Color888(r,g,b));
      matrix->setCursor(0,9);
      matrix->print(substring.c_str());
      matrix->setTextColor(matrix->Color888(50,0,0));
      matrix->swapBuffers(true);
      if(index <= textLen){
          index ++;
        }else{
          index = 1;
          substring = weather->descriptions;
        }
        //end descirptions
              delay(dly);


    }

    matrix->fillScreen(0);
    matrix->swapBuffers(true);
}
