#include <string>
#include <vector>
#ifndef RGBmatrixPanel4_H
#define RGBmatrixPanel4_H
#include <RGBmatrixPanel4.h>
#endif
#include <NtpClient.h>
#ifndef Structs_H
#define Structs_H
#include "./../structs.h"
#endif
void scrollingText(int r, int g, int b, int doubled,std::string text, RGBmatrixPanel4* matrix,int dly);
void loopingText(int r, int g, int b, int doubled,std::string text, int loops, RGBmatrixPanel4* matrix,int dly);
void loopedScrollingText(int r, int g, int b, int doubled,std::string text, int loops, RGBmatrixPanel4* matrix,int dly);
void scrollingTextChangeColor(std::string text, RGBmatrixPanel4* matrix,int dly, int r, int g, int b, int doubled, int rmode, int gmode, int bmode);
void displayUntilCancelled(int r, int g, int b,std::string text, RGBmatrixPanel4* matrix,int dly, std::string* activityFlag, std::string activity);
void displayClockUntilCancelled(int r, int g, int b,NTPClient timeClient, RGBmatrixPanel4* matrix,int dly, std::string* activityFlag, std::string activity);
void displayUntilCancelledEmphasised(int r, int g, int b,std::string text, RGBmatrixPanel4* matrix,int dly, std::string* activityFlag, std::string activity);
void displayOnce(int r, int g, int b,int* value, RGBmatrixPanel4* matrix);
void displayOnceUntilCancelled(int r, int g, int b,std::string text, RGBmatrixPanel4* matrix,int dly, std::string* activityFlag, std::string activity);
void displayWeather(int r, int g, int b, RGBmatrixPanel4* matrix,int dly, std::string* activityFlag, std::string activity, weatherStruct* weather);

