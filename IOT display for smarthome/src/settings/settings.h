#include <string>

std::string loadSettingFromJSONSettings(std::string setting);
void saveSettingToJSONSettings(std::string setting,std::string value);

std::string loadAllSettingsFromJSONSettings();
