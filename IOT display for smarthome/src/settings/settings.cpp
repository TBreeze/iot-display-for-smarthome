#include <string>
#include <SPIFFS.h>
#include <ArduinoJson.h>

std::string loadSettingFromJSONSettings(std::string setting){
  //get settings from document
  std::string error = "error";
  if(!SPIFFS.begin(true)){
    Serial.println("error initialising spiffs");
    std::string error = "error";
    return error;
  }
  File jsonSettingsFile = SPIFFS.open("/test.txt","r",true);
  if(!jsonSettingsFile){
    Serial.println("Error opening json settings.");
    return error;
  }
  String jsonSettingsSerialised = jsonSettingsFile.readString();
  jsonSettingsFile.close();
  //convert serialised string into json object
  DynamicJsonDocument doc(1024);
  if(!jsonSettingsSerialised){
    jsonSettingsSerialised = "{}";
  }
  deserializeJson(doc, jsonSettingsSerialised);
  JsonObject jsonSettings = doc.as<JsonObject>();
  return (jsonSettings[setting]);
}

void saveSettingToJSONSettings(std::string setting,std::string value){
    //retrieve current settings
    if(!SPIFFS.begin(true)){
        Serial.println("error initialising spiffs");
        return;
    }
    File jsonSettingsFile = SPIFFS.open("/test.txt","r",true);
    if(!jsonSettingsFile){
        Serial.println("Error opening json settings.");
        return ;
    }
    String jsonSettingsSerialised = jsonSettingsFile.readString();
    jsonSettingsFile.close();
    //convert serialised string into json object
    DynamicJsonDocument doc(1024);
    if(!jsonSettingsSerialised || jsonSettingsSerialised == "null"){
        jsonSettingsSerialised = "{}";
    }
    deserializeJson(doc, jsonSettingsSerialised);
    JsonObject jsonSettings = doc.as<JsonObject>();
    //add new/updated setting
    jsonSettings[setting] = value;
    jsonSettingsFile = SPIFFS.open("/test.txt","w+",true);
    if(!jsonSettingsFile){
        Serial.println("Error opening json settings.");
        return;
    }
    String json;
    serializeJson(jsonSettings, json);
    jsonSettingsFile.print(json);
    jsonSettingsFile.close();
}
std::string loadAllSettingsFromJSONSettings(){
  //get settings from document
  std::string error = "error";
  if(!SPIFFS.begin(true)){
    Serial.println("error initialising spiffs");
    std::string error = "error";
    return error;
  }
  File jsonSettingsFile = SPIFFS.open("/test.txt","r",true);
  if(!jsonSettingsFile){
    Serial.println("Error opening json settings.");
    return error;
  }
  String jsonSettingsSerialised = jsonSettingsFile.readString();
  jsonSettingsFile.close();
  //convert serialised string into json object
  if(!jsonSettingsSerialised){
    jsonSettingsSerialised = "{}";
  }
  return (jsonSettingsSerialised.c_str());
}


