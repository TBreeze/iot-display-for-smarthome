#include <string>
#include <vector>
      struct weatherStruct{
        std::string descriptions;
        std::string temp;
        std::string feels_like;
        std::string temp_min;
        std::string temp_max;
        std::string windspeed;
        std::string windgust;
        std::string sunrise;
        std::string sunset;
        std::string clouds;
        std::string rain;
        std::string snow;
        std::string location;
        int set;
      };