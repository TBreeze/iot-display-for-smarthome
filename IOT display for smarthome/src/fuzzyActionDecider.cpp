#include <vector>
#include <string>
#include "fuzzy.h"
#include <Arduino.h>
float  fuzzifyArray[6] = {};
float  inferenceArray[4] = {};

std::string decideAction(int countdown, int rss, int weather){
    fuzzify(countdown, rss, weather);
    int index=0;
    fuzzyInference();
    std::string action = defuzzify();
    return action;
}
// fuzzification
void fuzzify(int countdown, int rss, int weather){
    int countdownHighSize = 3;
    int countdownLowSize = 3;
    int countdownHigh [countdownHighSize][2]= {{50,100},{50,100},{40,100}};
    int countdownLow [countdownLowSize][2]= {{0,50},{0,50},{0,60}};

    int rssHighSize = 3;
    int rssLowSize = 3;
    int rssHigh [rssHighSize][2]= {{50,100},{50,100},{40,100}};
    int rssLow [rssLowSize][2]= {{0,50},{0,50},{0,60}};

    int weatherHighSize = 3;
    int weatherLowSize = 3;
    int weatherHigh [weatherHighSize][2]= {{50,100},{50,100},{40,100}};
    int weatherLow [weatherLowSize][2]= {{0,50},{0,50},{0,60}};

    fuzzifyArray[0] = getMembershipValue(countdownHighSize, countdown,countdownHigh);
    fuzzifyArray[2] = getMembershipValue(rssHighSize, rss,rssHigh);
    fuzzifyArray[4] = getMembershipValue(weatherHighSize, weather,weatherHigh);
    fuzzifyArray[1] = getMembershipValue(countdownLowSize, countdown,countdownLow);
    fuzzifyArray[3] = getMembershipValue(rssLowSize, rss,rssLow);
    fuzzifyArray[5] = getMembershipValue(weatherLowSize, weather,weatherLow);
}

float getMembershipValue(int size, int value, int membershipBoundaries[][2]){
    int index = 0;
    int membershipCount = 0;
    float membershipValue = 0;
    while(index < size){
        if(value >= membershipBoundaries[index][0]  && value <= membershipBoundaries[index][1]){
            membershipCount = membershipCount + 1;
        }
        index = index + 1;
    }
    if(membershipCount > 0){
    membershipValue = membershipCount / size;
    }
    return membershipValue;
}

void fuzzyInference(){
    float countdownshow = fuzzifyArray[0];
    float rssshow = fuzzifyArray[2];
    float weather = fuzzifyArray[4];
    float manualshow = max({fuzzifyArray[1],fuzzifyArray[3], fuzzifyArray[5]});
    inferenceArray[0] = countdownshow;
    inferenceArray[1] = rssshow;
    inferenceArray[2] = weather;
    inferenceArray[3] = manualshow;
}
// defussification

std::string defuzzify(){
    //max membership principle - defuzzification first of maxima
    int maxIndex = 0;
    int index = 0;

    while(index < 4){
        if(inferenceArray[maxIndex] < inferenceArray[index]){
            maxIndex = index;
        }
                    index = index +1;

    }

    std::string outputTypes[4]={"countdown", "rss", "weather", "manual"};

    return outputTypes[maxIndex];
}