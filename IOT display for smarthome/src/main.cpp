#include <NtpClient.h>
#include <Time.h>
#include <HTTPClient.h>
#include <ArduinoJson.h>
#include <tinyxml2.h>
#include "text/text.h"
#include "settings/settings.h"
#include <AsyncTCP.h>
#include <SPIFFS.h>
#include "html/html.h"
#include <string>
#include <WiFi.h>
#include <WiFiUdp.h>
#include <functions.h>
#include <bitset>
#include <LittleFS.h>
#include <FS.h>
#include "wifi/wifi.h"
#include "fuzzy.h"

#include <ArduinoJson.h>
#include <WiFiUdp.h>
#include <chrono>
#include <HTTPClient.h>
#ifndef Structs_H
#define Structs_H
#include "./structs.h"
#endif
// wofo close
//  This is an Arduino library for our 16x32 and 32x32 RGB LED matrix panels

// Pick one up at http://www.adafruit.com/products/420 & http://www.adafruit.com/products/607 !

// Written by Limor Fried/Ladyada & Phil Burgess/PaintYourDragon for Adafruit Industries.
// BSD license, all text above must be included in any redistribution
#ifndef RGBmatrixPanel4_H
#define RGBmatrixPanel4_H
#include <RGBmatrixPanel4.h>
#endif

using namespace tinyxml2;

// GPIO pin constants
#define CLK 14 // USE THIS ON ESP32
#define OE 13
#define LAT 15
#define A 26
#define B 4
#define C 27
#define BLEFT 34
#define BUP 35
#define BDOWN 33

std::map<std::string, int> activityHormoneValues = {{"countdown", 0}, {"rss", 0}, {"weather", 0}};

// Last parameter = 'true' enables double-buffering, for flicker-free,
// buttery smooth animation.  Note that NOTHING WILL SHOW ON THE DISPLAY
// until the first call to swapBuffers().  This is normal.
RGBmatrixPanel4 matrix(A, B, C, CLK, LAT, OE, true, 2);
// Double-buffered mode consumes nearly all the RAM available on the
// Arduino Uno -- only a handful of free bytes remain.

// Task handles
TaskHandle_t Task1, Task2;
// default color values
int rmode = 1;
int gmode = 0;
int bmode = 0;
// bitmap corresponding values
int red[4] = {0, 8, 12, 15}; // after 15 pixels ghost
int blue[4] = {0, 32, 79, 127};
int green[8] = {0, 8, 20, 42, 54, 68, 80, 95};
int dly = 200;
// matrix dimensions
int matrixXPixels = 64;
int matrixYPixels = 16;
std::string activity = "booting";
std::string offline_message;
std::string online_string;
weatherStruct weather;

std::string local_ip;
WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP);
int countdownLeft;
int countdownActive = 0;
struct tm countdownEndG;
std::string lastrssdate;
XMLDocument xmlDocument;
int rssindex = 0;
int aimode = 1;
JsonObject jsonWeather;
DynamicJsonDocument weatherDoc(1024);
time_t countdownHormoneLast;
void setup()
{
  Serial.begin(9600);
  matrix.begin();
  matrix.setTextWrap(false); // Allow text to run off right edge
  xTaskCreatePinnedToCore(
      matrixCore,
      "Task_1",
      10000,
      NULL,
      1,
      &Task1,
      0);
  xTaskCreatePinnedToCore(
      managementCore,
      "Task_2",
      30000,
      NULL,
      1,
      &Task2,
      1);
}

void managementCore(void *parameter)
{

  activity = "booting";

  activity = "connecting to wifi";
  AsyncWebServer server(80);
  // connectToWifi(server);
  DNSServer dns;
  AsyncWiFiManager wifiManager(&server, &dns);
  // wifiManager.resetSettings();
  wifiManager.setConfigPortalTimeout(300);
  wifiManager.autoConnect("ESP", "123456789");
  if (!SPIFFS.begin(true))
  {
    Serial.println("An Error has occurred while mounting SPIFFS");
    return;
  }
  std::string aimoderaw = loadSettingFromJSONSettings("ai_mode");

  online_string = loadSettingFromJSONSettings("online_string");

  if (aimoderaw == "null")
  {
    aimode = 0;
  }
  else
  {
    aimode = stoi(aimoderaw);
  }

  activity = "";
  while (true)
  {
    if (WiFi.isConnected())
    {
      server.on("/update/offlinestring", HTTP_GET, [](AsyncWebServerRequest *request)
                {
        if (request->hasParam("value") ){
            std::string value = request->getParam("value")->value().c_str();
            saveSettingToJSONSettings("offline_string",value);
            request->send(200);   
        } 
        request->send(404); });
      server.on("/update/onlinestring", HTTP_GET, [](AsyncWebServerRequest *request)
                {
        if (request->hasParam("value") ){
            std::string value = request->getParam("value")->value().c_str();
            saveSettingToJSONSettings("online_string",value);
            online_string = value;
            activity="online_string";
            request->send(200);   
        } 
        request->send(404); });
      server.on("/settings", HTTP_GET, [](AsyncWebServerRequest *request)
                { request->send(200, "application/json", loadAllSettingsFromJSONSettings().c_str()); });
      server.on("/clock/display", HTTP_GET, [](AsyncWebServerRequest *request)
                {
      activity="clock";
        request->send(200); });
      server.on("/", HTTP_GET, [](AsyncWebServerRequest *request)
                {
        std::string test = home();
        request->send(200, "text/html", test.c_str()); });
      server.on("/start/countdown", HTTP_GET, [](AsyncWebServerRequest *request)
                {
        if (request->hasParam("value") ){
            std::string value = request->getParam("value")->value().c_str();
            int countdown = stoi(value);
            // struct tm epoch_time;
            long epochval = timeClient.getEpochTime();
            time_t seconds = epochval + countdown;
            countdownLeft = countdown;
            countdownActive = 1;
            struct tm countdownEnd;
            memcpy(&countdownEnd, localtime(&seconds), sizeof(struct tm));
            countdownEndG = countdownEnd;
                        activity="countdown";
                                    request->send(200);   


        } 
        request->send(404); });
      server.on("/start/rss", HTTP_GET, [](AsyncWebServerRequest *request)
                {
        if (request->hasParam("value") ){
            std::string value = request->getParam("value")->value().c_str();
            saveSettingToJSONSettings("rss_bbc",value);
            rssindex = 0;
            activity="rssloading";

            getRssFeed();

            activity="rss";
                        request->send(200);   

        } 
        request->send(404); });
      server.on("/start/weather", HTTP_GET, [](AsyncWebServerRequest *request)
                {
        if (request->hasParam("lon") && request->hasParam("lat")){
            std::string lon = request->getParam("lon")->value().c_str();
            std::string lat = request->getParam("lat")->value().c_str();
            saveSettingToJSONSettings("weather_lat",lat);
            saveSettingToJSONSettings("weather_lon",lon);
            activity = "weatherloading";
            getWeather();
            activity = "weather";
            request->send(200);   

        } 
        request->send(404); });
      server.on("/ai/mode", HTTP_GET, [](AsyncWebServerRequest *request)
                {
                  if (request->hasParam("value"))
                  {

                    std::string value = request->getParam("value")->value().c_str();
                    std::string valuei = "0";
                    if (value == "on")
                    {
                      valuei = "1";
                    }
                    else if (value == "off")
                    {
                      valuei = "0";
                    }

                    saveSettingToJSONSettings("ai_mode", valuei);

                    aimode = stoi(valuei);

                    request->send(200);
                  }
                  request->send(404); });
      server.begin();
      timeClient.begin();
      timeClient.setTimeOffset(3600);

      while (!timeClient.update())
      {
        timeClient.forceUpdate();
      }

      activity = "connected";
      clock_t last_clicked;
      while (true)
      {
        if (((clock() - last_clicked) / 1000) > 10)
        {
          break;
        }
      }
      local_ip = WiFi.localIP().toString().c_str();
      activity = "display_ip";
      last_clicked = clock();
      while (true)
      {
        if (((clock() - last_clicked) / 1000) > 10)
        {
          break;
        }
      }
      activity = "clock";
      while (true)
      {
        if (aimode == 1)
        {
          getRssFeed();
          getWeather();
          limitHormones();
          std::string fuzzyActivity = decideAction(activityHormoneValues["countdown"], activityHormoneValues["rss"], activityHormoneValues["weather"]);
          if (fuzzyActivity != "manual")
          {
            activity = fuzzyActivity;
            activityHormoneValues[fuzzyActivity] = 0;
          }
        }
        processCountdownActive();

        if (countdownActive == 1 && activity == "countdownfinished")
        {
          clock_t last_clicked = clock();
          while (true)
          {
            if (((clock() - last_clicked) / 1000) > 10)
            {
              countdownActive = 0;
              activity = "clock";
              break;
            }
          }
        }
        if (activity == "weather")
        {
          getWeather();
        }

        delay(1000);
      }
    }
    else
    {
      noWifi();
    }
  }
  vTaskSuspend(Task2);
}
void limitHormones()
{
  if (activityHormoneValues["countdown"] > 100)
  {
    activityHormoneValues["countdown"] = 100;
  }
  if (activityHormoneValues["rss"] > 100)
  {
    activityHormoneValues["rss"] = 100;
  }
  if (activityHormoneValues["weather"] > 100)
  {
    activityHormoneValues["weather"] = 100;
  }
}
void countdownHormoneIncrease(time_t current)
{
  if (countdownHormoneLast == 0)
  {
    countdownHormoneLast = current;
  }
  else if (current - countdownHormoneLast > 1)
  {
    activityHormoneValues["countdown"] = activityHormoneValues["countdown"] + (current - countdownHormoneLast);

    countdownHormoneLast = current;

    Serial.println(activityHormoneValues["countdown"]);
  }
}
void processCountdownActive()
{
  if (countdownActive == 1)
  {
    time_t epochEnd = mktime(&countdownEndG);
    time_t epochNow = timeClient.getEpochTime();
    if ((epochEnd - epochNow) < 0)
    {
      activity = "countdownfinished";
      activityHormoneValues["countdown"] = 0;
    }
    else
    {
      countdownLeft = epochEnd - epochNow;
      countdownHormoneIncrease(epochNow);
    }
  }
}
void getWeather()
{
  std::string lat = loadSettingFromJSONSettings("weather_lat");
  std::string lon = loadSettingFromJSONSettings("weather_lon");
  std::string url = "https://api.openweathermap.org/data/2.5/weather?lat=" + lat + "&lon=" + lon + "&appid=1bb5e54029c8ec0892f02aeeb698f5fc";
  HTTPClient http;
  http.begin(url.c_str());
  int httpCode = http.GET();
  if (httpCode > 0)
  {
    std::string weatherpay = http.getString().c_str();
    http.end();
    deserializeJson(weatherDoc, weatherpay);
    jsonWeather = weatherDoc.as<JsonObject>();
    int index = 0;
    std::string descriptions;
    while (true)
    {
      std::string description = jsonWeather["weather"][index]["description"];
      if (description == "null")
      {
        break;
      }
      descriptions = descriptions + description + " - ";
      index = index + 1;
    }
    weather.descriptions = descriptions;
    std::string temp = jsonWeather["main"]["temp"];
    if (temp != weather.temp)
    {
      activityHormoneValues["weather"] = activityHormoneValues["weather"] + 10;
    }
    weather.temp = temp;
    std::string feels_like = jsonWeather["main"]["feels_like"];
    if (feels_like != weather.feels_like)
    {
      activityHormoneValues["weather"] = activityHormoneValues["weather"] + 10;
    }
    weather.feels_like = feels_like;
    std::string temp_min = jsonWeather["main"]["temp_min"];
    weather.temp_min = temp_min;
    std::string temp_max = jsonWeather["main"]["temp_max"];
    weather.temp_max = temp_max;
    std::string windspeed = jsonWeather["wind"]["speed"];
    if (windspeed != weather.windspeed)
    {
      activityHormoneValues["weather"] = activityHormoneValues["weather"] + 10;
    }
    weather.windspeed = windspeed;
    std::string windgust = jsonWeather["wind"]["gust"];
    weather.windgust = windspeed;
    std::string sunrise = jsonWeather["sys"]["sunrise"];
    weather.sunrise = sunrise;
    std::string sunset = jsonWeather["sys"]["sunset"];
    weather.sunset = sunset;
    std::string clouds = jsonWeather["clouds"]["all"];
    if (clouds != weather.clouds)
    {
      activityHormoneValues["weather"] = activityHormoneValues["weather"] + 10;
    }
    weather.clouds = clouds;
    std::string rain = jsonWeather["rain"]["1h"];
    weather.rain = rain;
    if (rain != weather.rain)
    {
      activityHormoneValues["weather"] = activityHormoneValues["weather"] + 10;
    }
    std::string snow = jsonWeather["snow"]["1h"];
    if (snow != weather.snow)
    {
      activityHormoneValues["weather"] = activityHormoneValues["weather"] + 10;
    }
    weather.snow = snow;
    std::string location = jsonWeather["name"];
    weather.location = location;
    weather.set = 1;
  }
  else
  {
    http.end();
    Serial.println("could not get weather");
    return;
  }
}
std::string rssItemtodisplay()
{
  if (lastrssdate != "")
  {
    XMLElement *el = xmlDocument.FirstChildElement("rss")->FirstChildElement("channel")->FirstChildElement("item");
    if (rssindex != 0)
    {
      int i = 0;
      while (i < rssindex)
      {
        el = el->NextSiblingElement();
        i = i + 1;
      }
    }
    std::string item = el->FirstChildElement("title")->FirstChild()->ToText()->Value();
    std::string last = xmlDocument.FirstChildElement("rss")->FirstChildElement("channel")->LastChildElement("item")->FirstChildElement("title")->FirstChild()->ToText()->Value();
    if (item != last)
    {
      rssindex = rssindex + 1;
    }
    else
    {
      rssindex = 0;
      getRssFeed();
    }
    return item;
  }
  else
  {
    activity = "rssCantload";
    return "";
  }
}

void matrixCore(void *parameter)
{
  while (true)
  {
    processCountdownActive();
    if (activity == "booting")
    {
      displayUntilCancelled(red[0], green[0], blue[1], "Booting...", &matrix, dly, &activity, activity);
    }
    else if (activity == "connecting to wifi")
    {
      displayUntilCancelled(red[0], green[0], blue[1], "Connecting to wifi...", &matrix, dly, &activity, activity);
    }
    else if (activity == "no wifi")
    {
      displayUntilCancelled(red[0], green[0], blue[1], "No Wifi... Restart System To Try Again.", &matrix, dly, &activity, activity);
    }
    else if (activity == "connected")
    {
      displayUntilCancelled(red[0], green[0], blue[1], "Connected...", &matrix, dly, &activity, activity);
    }
    else if (activity == "offline text")
    {
      displayUntilCancelled(red[0], green[0], blue[1], offline_message, &matrix, dly, &activity, activity);
    }
    else if (activity == "online_string")
    {
      displayUntilCancelled(red[0], green[0], blue[1], online_string, &matrix, dly, &activity, activity);
    }
    else if (activity == "display_ip")
    {
      displayUntilCancelled(red[0], green[0], blue[1], local_ip, &matrix, dly, &activity, activity);
    }
    else if (activity == "clock")
    {
      displayClockUntilCancelled(red[0], green[0], blue[1], timeClient, &matrix, dly, &activity, activity);
    }
    else if (activity == "countdown")
    {
      displayOnce(red[0], green[0], blue[1], &countdownLeft, &matrix);
    }
    else if (activity == "countdownfinished")
    {
      displayUntilCancelledEmphasised(red[0], green[0], blue[1], "countdown Finished!", &matrix, dly, &activity, activity);
    }
    else if (activity == "rss")
    {
      std::string rsstodisplay = rssItemtodisplay();
      displayOnceUntilCancelled(red[0], green[0], blue[1], rsstodisplay, &matrix, dly, &activity, activity);
    }
    else if (activity == "rssloading")
    {
      displayUntilCancelled(red[0], green[0], blue[1], "Rss Loading...", &matrix, dly, &activity, activity);
    }
    else if (activity == "rssCantload")
    {
      displayUntilCancelled(red[0], green[0], blue[1], "Rss Couldnt Load. Please try again.", &matrix, dly, &activity, activity);
    }
    else if (activity == "weather")
    {
      if (weather.set == 1)
      {
        displayWeather(red[0], green[0], blue[1], &matrix, dly, &activity, activity, &weather);
      }
      else
      {
        activity = "weatherCantLoad";
      }
    }
    else if (activity == "weatherloading")
    {
      displayUntilCancelled(red[0], green[0], blue[1], "Weather Loading...", &matrix, dly, &activity, activity);
    }
    else if (activity == "weatherCantload")
    {
      displayUntilCancelled(red[0], green[0], blue[1], "Weather Couldnt Load. Please try again.", &matrix, dly, &activity, activity);
    }
    else
    {
      activity = "sleeping";
      displayUntilCancelled(red[0], green[0], blue[1], "Sleeping...", &matrix, dly, &activity, activity);
    }
  }
  vTaskSuspend(Task1);
}

void getRssFeed()
{
  std::string url = "http://feeds.bbci.co.uk/news/rss.xml#";
  HTTPClient http;
  http.begin(url.c_str());
  int httpCode = http.GET();
  if (httpCode > 0)
  {
    xmlPayload = http.getString().c_str();
    XMLError test;
    // seems to be an error on slow connections such as phone hotspot
    try
    {
      test = xmlDocument.Parse(xmlPayload);
    }
    catch (...)
    {
      Serial.print("caught");
      Serial.println(xmlDocument.ErrorIDToName(test));
      lastrssdate = "";
      http.end();

      return;
    }
    if (test == XML_SUCCESS)
    {
      http.end();
      std::string builddate = xmlDocument.FirstChildElement("rss")->FirstChildElement("channel")->FirstChildElement("lastBuildDate")->GetText();
      Serial.println(builddate.c_str());

      if (lastrssdate != builddate)
      {
        lastrssdate = builddate;
        activityHormoneValues["rss"] = activityHormoneValues["rss"] + 10;
        rssindex = 0;
      }
      return;
    }
    else
    {
      lastrssdate = "";
      http.end();
      return;
    }
  }
  else
  {
    lastrssdate = "";
    return;
  }
}
void noWifi()
{
  activity = "no wifi";
  clock_t last_clicked;
  while (true)
  {
    if (((clock() - last_clicked) / 1000) > 10)
    {
      break;
    }
  }
  std::string offline_string = loadSettingFromJSONSettings("offline_string");
  if (offline_string == "null")
  {
    offline_string = "Connect to a network to display a custom offline message!";
  }
  offline_message = offline_string;
  while (true)
  {
    activity = "offline text";
  }
}

// void codeForTask2(void *parameter){
//     clock_t last_clicked;
//     last_clicked = clock();
//   pinMode(BUP, INPUT);
//       pinMode(BDOWN, INPUT);
//       pinMode(BLEFT, INPUT);
//   while(true){
//     double difference = clock() - last_clicked;
//     float seconds = difference / 500;
//       int down=digitalRead(BDOWN);
//       if(down == 0){
//         if( seconds > 1 ){
//         rmode ++;
//         if(rmode > 3){
//           rmode = 0;
//         }
//           last_clicked = clock();

//         }
//       }
//       int up=digitalRead(BUP);
//       if(up == 0){
//         if( seconds > 1 ){
//         gmode ++;
//         if(gmode > 7){
//           gmode = 0;
//         }
//           last_clicked = clock();
//         }
//       }
//       int left=digitalRead(BLEFT);
//       if(left == 0){
//         if( seconds > 1 ){
//         bmode ++;
//         if(bmode > 3){
//           bmode = 0;
//         }
//           last_clicked = clock();
//         }
//       }

//   }
//   vTaskSuspend(Task2);
// }
// void matrixDisplay(void *parameter){
//       //convert 8 digit binary to base 10
//       //int i = std::stoi("10110010", nullptr, 2); //enter 8 didget binary
//       //convert base 10 to binary
//       //std::string binary = std::bitset<8>(178).to_string();

//       uint8_t bitmap[matrixYPixels][matrixXPixels] = {{0,0,0,0,64,0,0,64,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
//       {0,0,0,0,64,0,0,64,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
//       {0,0,0,0,64,0,0,64,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
//       {0,0,0,0,64,0,0,64,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,64,64,64,0,0,0,0,0,0,64,64,64,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
//       {0,0,0,0,64,0,0,64,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,64,64,64,0,0,0,0,0,0,64,64,64,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
//       {0,0,0,0,64,0,0,64,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,64,64,64,0,0,0,0,0,0,64,64,64,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
//       {0,0,0,0,64,0,0,64,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
//       {0,0,0,0,64,0,0,64,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
//       {0,0,0,0,64,0,0,64,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,72,72,72,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
//       {0,0,0,0,64,0,0,64,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,72,72,72,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
//       {0,0,0,0,64,0,0,64,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,72,72,72,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
//       {0,0,0,0,64,0,0,64,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
//       {0,0,0,0,64,0,0,64,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
//       {0,0,0,0,64,0,0,64,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
//       {0,0,0,0,64,0,0,64,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,64,64,64,64,64,64,64,64,64,64,64,64,64,64,64,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
//       {0,0,0,0,64,0,0,64,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,64,64,64,64,64,64,64,64,64,64,64,64,64,64,64,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},

//       };
//        int y = 0;
//        int x = 0;
//        // while(y < matrixYPixels){
//        //   while(x< matrixXPixels){
//        //     bitmap[y][x] = static_cast<uint8_t>(64);
//        //     x = x +1;
//        //   }
//        //   x=0;
//        //   y = y +1;
//        // }
//        // y = 0;
//        // x = 0;
//        while(y < matrixYPixels){
//          while(x< matrixXPixels){
//           long test = bitmap[y][x];
//           std::string binary = std::bitset<8>(test).to_string();
//           std::string rbin = binary.substr(0, 2);
//           std::string gbin = binary.substr(2, 3);
//           std::string bbin = binary.substr(5, 2);
//           std::string dbin = binary.substr(7, 1);
//           int rval = std::stoi(rbin, nullptr, 2);
//           int gval = std::stoi(gbin, nullptr, 2);
//           int bval = std::stoi(bbin, nullptr, 2);
//           int doubled = std::stoi(dbin, nullptr, 2);

//           int r = red[rval] * (doubled+1);
//           int g = green[gval] * (doubled+1);
//           int b = blue[bval] * (doubled+1);
//           matrix.drawPixel(x,y,
//           matrix.Color888(r,g,b)          );
//            x = x +1;

//            }

//          x=0;
//          y = y +1;
//        }
//        matrix.swapBuffers(true);
//       matrix.fillScreen(0);
//       delay(8000);

//        int doubled = 0;
//        scrollingText(red[rmode],green[gmode],blue[bmode], doubled, "This is scrolling text.", &matrix, dly);
//        loopingText(red[rmode],green[gmode],blue[bmode], doubled, "This is looping text.", 2, &matrix, dly);
//        loopedScrollingText(red[rmode],green[gmode],blue[bmode], doubled, "This is looped scrolling text.", 2, &matrix,dly);
//        while(true){
//          scrollingTextChangeColor("This is scrolling text.", &matrix, dly, red[rmode],green[gmode],blue[bmode], doubled, rmode, gmode, bmode);
//        }

//       vTaskSuspend(Task1);

// }

void loop()
{
}