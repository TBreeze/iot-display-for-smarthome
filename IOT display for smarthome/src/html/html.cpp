#include <string>

std::string home(){
    std::string home = 
    "<!DOCTYPE html >"
    "<head>"
    "<script>"
    "function offlineTextSubmit(){let stringEl=document.getElementById('offlineTextInput'); console.log(stringEl.value);var xhttp = new XMLHttpRequest(); xhttp.open('GET', '/update/offlinestring?value='+stringEl.value, true);xhttp.send();}"
    "function onlineTextSubmit(){let stringEl=document.getElementById('onlineTextInput'); console.log(stringEl.value);var xhttp = new XMLHttpRequest(); xhttp.open('GET', '/update/onlinestring?value='+stringEl.value, true);xhttp.send();}"

    "function loadData(){var xhttp = new XMLHttpRequest();xhttp.open('GET', '/settings', true);xhttp.send();xhttp.onreadystatechange=function(){if(xhttp.readyState == XMLHttpRequest.DONE){let obj = JSON.parse(xhttp.responseText); document.getElementById('offlineTextInput').value = obj.offline_string; document.getElementById('bbcrssinput').value = obj.rss_bbc;}}}"
    "function startcountdown(){let stringEl=document.getElementById('countdownSecondsInput'); console.log(stringEl.value);var xhttp = new XMLHttpRequest(); xhttp.open('GET', '/start/countdown?value='+stringEl.value, true);xhttp.send();}"
    "function aimodeon(){var xhttp = new XMLHttpRequest(); xhttp.open('GET', '/ai/mode?value=on', true);xhttp.send();}"
    "function aimodeoff(){var xhttp = new XMLHttpRequest(); xhttp.open('GET', '/ai/mode?value=off', true);xhttp.send();}"
    "function clockDisplay(){var xhttp = new XMLHttpRequest(); xhttp.open('GET', '/clock/display', true);xhttp.send();}"
    "function rssDisplay(){let stringEl=document.getElementById('bbcrssinput'); console.log(stringEl.value);var xhttp = new XMLHttpRequest(); xhttp.open('GET', '/start/rss?value='+stringEl.value, true);xhttp.send();}"
    "function weatherDisplay(){let stringElLat=document.getElementById('weatherlatinput');let stringElLong=document.getElementById('weatherlonginput');var xhttp = new XMLHttpRequest(); xhttp.open('GET', '/start/weather?lat='+stringElLat.value+'&lon='+stringElLong.value, true);xhttp.send();}"

    "</script>"
    "</head>"
    "<body onload='loadData();'>"
    "<h1>Offline Text String</h1>"
    "<input id='offlineTextInput' type='text' placeholder='The message you want displayed in offline mode.'></input>"
        "<button onclick='offlineTextSubmit()'>Update offline text string.</button>"

    "<h1>Display Text String</h1>"
    "<input id='onlineTextInput' type='text' placeholder='The message you want displayed now.'></input>"
    "<button onclick='onlineTextSubmit()'>Update online text string.</button>"
    "<h1>countdown</h1>"
    "<button onclick='startcountdown()'>Start countdown.</button>"
    "<input id='countdownSecondsInput' placeholder='The length in seconds for the countdown'></input>"
    "<h1>AI</h1>"
    "<button onclick='aimodeon()'>On.</button>"
    "<button onclick='aimodeoff()'>Off.</button>"
    "<h1>Display rss</h1>"
    "<input id='bbcrssinput' placeholder='The bbc rss feed'></input>"
    "<button onclick='rssDisplay()'>BBC rss.</button>"
    "<h1>Clock</h1>"
    "<button onclick='clockDisplay()'>Display Clock.</button>"
    "<h1>Weather</h1>"
    "<p>lat</p>"
    "<input id='weatherlatinput' placeholder='Latitude'></input>"
    "<p>long</p>"
    "<input id='weatherlonginput' placeholder='longitude'></input>"
    "<div><button onclick='weatherDisplay()'>Display weather.</button></div>"
    "</body>"
    "</html>"
    ;
    return home;
}
